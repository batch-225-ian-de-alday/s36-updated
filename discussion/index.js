// Setup the dependencies

const express = require('express');
const mongoose = require('mongoose');
const dotenv = require("dotenv").config(); // for password encrypting

const taskRoute = require("./routes/taskRoute")


/* // another way on putting password in the database instead of using env
const prompt = require('prompt-sync')(); // prompt for password

const name = prompt('What is your password?');// prompt for password
console.log(`Hey there is your password ${name}`);// prompt for password
 */

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// connnect to MongoDB atas
// process.env.PASSWORD if you want to use the env file on your password
mongoose.connect(`mongodb+srv://iandealday:${process.env.PASSWORD}@cluster0.qmugmsf.mongodb.net/s36-MRC?retryWrites=true&w=majority`, 
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);


// Set up notification for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log('Connected to MongoDB'));

// Add the task routes
// allows all the task  routes created in the "taskRoutes.js" file to use " /tasks " route

app.use("/tasks", taskRoute);


// Port Connect
app.listen(port, () => console.log(`Now listening to port ${port}`));





