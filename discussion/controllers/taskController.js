// Controllers contain the functions and business logic of our express JS. Meaning all the operation it can do will be placed this file.

const Task = require("../models/task")


module.exports.getAllTasks = () => {

    // The "return" statement. returns the result of the Mongoose method
    // Then ".then" method is used to wait for the Monggoose method to finish before sending result back to the route.
    
    return Task.find({}).then(result => {
        return result;
    })
};

module.exports.createTask = (requestBody) => {

    // Create a task object based on the Mongoose Model "task"
    let newTask = new Task({

        name : requestBody.name
    })

    return newTask.save().then((task, error) => {
        if (error) {
            console.log(error);

            // if an error is encountered, the "return" will prevent any other line or code within the same code block
            // The else statement will no longer be evaluated
            return false;

        } else {
            return task;
        }

    })  
};

module.exports.deleteTask = (taskId) => {

    // The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
    return Task.findByIdAndRemove(taskId).then((result, err) => {
        
        if (err) {

            console.log(err);
            return false;

        } else {
            return result
        }
    })
}; 


module.exports.updateTask = (taskId, newContent) => {

    return Task.findById(taskId).then((result, err) => {

            if (err) {
                console.log(err)
                return false;

            } else {

                result.name = newContent.name;
        
                return result.save().then((updated, saveErr) => {
                    if (saveErr) {
                        console.log(saveErr);
                        return false;

                    } else {

                        return updated;
                }
                
            })
        }
    })
}


// ====================================================
// Activity 

// Get specific task by ID 
module.exports.getSpecificTask = (taskId) => {

    return Task.findById(taskId).then((result, err) => {
        
        if (err) {
    
            console.log(err);
            return false;
    
        } else {
            return result
        }
    })
   
};


// for complete status
// note that the return in postman is still the old value that will be change but if you check again in the get info it is already updated. 
module.exports.updateStatusByID = (taskId) => {
    return Task.findByIdAndUpdate(taskId,{"status" : "Complete"}).then((result, err) => {
        
        if (err) {
            console.log(err);
            return false;

        } else {
            return result;
        }
    })
}; 

// for complete status
// another method this returns the updated value in postman
/* module.exports.updateStatusByID = (taskId, newStatus) => {

    return Task.findById(taskId).then((result, err) => {

            if (err) {
                console.log(err)
                return false;

            } else {

                result.password = "";
                newStatus.status = result.status;
        
                return result.save().then((updated, saveErr) => {
                    if (saveErr) {
                        console.log(saveErr);
                        return false;

                    } else {

                        return updated;
                }
                
            })
        }
    })
}; */


// for pending status
module.exports.updateStatusByIDtoDefault = (taskId, newStatus) => {
    return Task.findByIdAndUpdate(taskId,{"status" : "Pending"}).then((result, err) => {
        
        if (err) {
            console.log(err);
            return false;

        } else {
            newStatus = result;
            return newStatus;
        }
    })
}; 