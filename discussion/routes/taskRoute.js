// Contains all the endpoints for our application

// We need to use express' Router() function to achieve this
const express = require("express");

// Creates a router instance that functions as a middleware and routing system
// Allow access to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router();

const taskController = require("../controllers/taskController");
const task = require("../models/task");

/* 
    Syntax : localhost:3001/tasks/getinfo
*/

// [SECTION] Routes for GET all task
router.get("/getinfo", (req,res) => {

    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

})

// Route to CREATE a new task
router.post("/create", (req,res) => {
   
    // if information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})


// Route to DELETE a task
// the colon (:) is an idetifier that helps create a dynamic route which allows us to supply information in the URL
// the word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
/* 
    Ex.
    localhost:3001/tasks/delete/63c67aa89549cb8b28462fc8
    the 63c67aa89549cb8b28462fc8 is assigned to the "id" parameter in the url
    id comes from the database 
*/

router.delete("/delete/:id", (req,res) => {

    // URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	// If information will be coming from the URL, the data can be accessed from the request "params" property
    taskController.deleteTask(req.params.id).then((result) => res.send(result))
});


// Route to update task using PUT Method

router.put("/update/:id", (req,res) => {
    
    taskController.updateTask(req.params.id, req.body).then((result) => res.send(result));
})


// ==================================================
// Activity 

// Route to a specific ID using GET method
router.get("/:id", (req,res) => {
    taskController.getSpecificTask(req.params.id).then((result) => res.send(result));
});


// Route to updating status to "complete" by ID using PUT Method
router.put("/:id/complete", (req,res) => {
    taskController.updateStatusByID(req.params.id).then((result) => res.send(result));
})


router.put("/:id/default", (req,res) => {
    taskController.updateStatusByIDtoDefault(req.params.id).then((result) => res.send(result));
})



// Use "module.exports" to export the router object to use in the "app.js"

module.exports = router;